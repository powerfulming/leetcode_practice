#include <iostream>
#include <vector>

using namespace std;

struct ListNode
{
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution
{
public:
    ListNode *mergeTwoLists(ListNode *list1, ListNode *list2)
    {
        ListNode *tmpList1 = list1;
        ListNode *tmpList2 = list2;
        ListNode *head = new ListNode();
        ListNode *mergedList = new ListNode();
        head->next = mergedList;

        while (tmpList1 != nullptr && tmpList2 != nullptr)
        {
            if (tmpList1->val < tmpList2->val)
            {
                mergedList->val = tmpList1->val;
                ListNode *tmpNode = new ListNode();
                mergedList->next = tmpNode;
                mergedList = mergedList->next;
                if (tmpList1->next != nullptr)
                {
                    tmpList1 = tmpList1->next;
                }
                else
                {
                    break;
                }
            }
            else
            {
                mergedList->val = tmpList2->val;
                ListNode *tmpNode = new ListNode();
                mergedList->next = tmpNode;
                mergedList = mergedList->next;
                if (tmpList2->next != nullptr)
                {
                    tmpList2 = tmpList2->next;
                }
                else
                {
                    break;
                }
            }
        }
        while (tmpList1 != nullptr)
        {
            mergedList->val = tmpList1->val;
            if (tmpList1->next != nullptr)
            {
                ListNode *tmpNode = new ListNode();
                mergedList->next = tmpNode;
                tmpList1 = tmpList1->next;
            }
            else
            {
                break;
            }
        }
        while (tmpList2 != nullptr)
        {
            mergedList->val = tmpList2->val;
            if (tmpList2->next != nullptr)
            {
                ListNode *tmpNode = new ListNode();
                mergedList->next = tmpNode;
                tmpList2 = tmpList2->next;
            }
            else
            {
                break;
            }
        }

        return head->next;
    }
};

void print_list(ListNode *list)
{
    ListNode *newNode = list;
    while (newNode->next != nullptr)
    {
        cout << newNode->val << ", ";
        newNode = newNode->next;
    }
    cout << newNode->val << endl;
}

ListNode *create_list(vector<int> myVec)
{
    ListNode *myNode1 = new ListNode();
    for (int i = myVec.size() - 1; i >= 0; i--)
    {
        myNode1->val = myVec[i];
        ListNode *tmpNode = new ListNode();
        tmpNode->next = myNode1;
        myNode1 = tmpNode;
    }
    return myNode1->next;
}

int main(int argc, char **argv)
{
    // ListNode *myNode = new ListNode(1);
    // for(size_t i=0; i<3 ;i++)
    // {
    //     ListNode *newNode = new ListNode(i+1);
    //     newNode->next = myNode;
    //     myNode = newNode;
    // }
    // print_list(myNode);
    vector<int> vec1 = {1, 3, 4};
    vector<int> vec2 = {1, 2, 4};
    ListNode *myList1 = create_list(vec1);
    ListNode *myList2 = create_list(vec2);

    // print_list(myList1);
    // print_list(myList2);

    Solution sol;
    ListNode *answer = sol.mergeTwoLists(myList1, myList2);
    print_list(answer);
    return 0;
}