#include <iostream>
#include <vector>
#include <set>
#include <algorithm>

using namespace std;

class Solution
{
public:
    void printAnswer(vector<vector<int>> input)
    {
        for (auto vec : input)
        {
            for (auto num : vec)
            {
                cout << num << " ";
            }
            cout << endl;
        }
    }

    vector<vector<int>> threeSum(vector<int> &nums)
    {
        set<vector<int>> answer;
        vector<vector<int>> answerV;
        if (nums.size() == 0)
        {
            return answerV;
        }
        else if (nums.size() < 3)
        {
            return answerV;
        }
        else
        {
        }
        sort(nums.begin(), nums.end());

        bool firstTime = true;
        for (size_t i = 0; i < nums.size() - 2; i++)
        {
            if(!firstTime)
            {
                while(i<nums.size()-2 && nums[i] == nums[i-1])
                {
                    i++;
                }
            }
            firstTime = false;
            size_t j = i + 1;
            size_t k = nums.size() - 1;

            while (j < k)
            {
                if ((nums[i] + (nums[j] + nums[k])) < 0)
                {
                    j++;
                    continue;
                }
                else if ((nums[i] + (nums[j] + nums[k])) > 0)
                {
                    k--;
                    continue;
                }
                else
                {
                    vector<int> tmpVec = {nums[i], nums[j], nums[k]};
                    // answer.insert(tmpVec);
                    answerV.push_back(tmpVec);
                    j++;
                    k--;
                    // Double check the coming up number is same or not
                    while(j<k && nums[j]==nums[j-1])
                        j++;
                    while(j<k && nums[k]==nums[k+1])
                        k--;
                    continue;
                }
            }
        }
        // copy(answer.begin(), answer.end(), back_inserter(answerV));
        return answerV;
    }
    vector<vector<int>> sortVec_;
};
//  0,  1,  2, 3, 4, 5
// -4, -1, -1, 0, 1, 2

int main(int argc, char **argv)
{
    // vector<int> testSet1 =  {-2, 13, -5, -4, -7, 8, 0, -9, 6, 7, 0, -4, 2, 1, -2, 4};
    // vector<int> testSet1 =  {-1,0,1,2,-1,-4};
    vector<int> testSet1 =  {-2,0,1,1,2};   // [[-2,0,2],[-2,1,1]]
    Solution sol;
    auto answer = sol.threeSum(testSet1);
    sol.printAnswer(answer);
    return 0;
}