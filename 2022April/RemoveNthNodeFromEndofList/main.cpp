#include <vector>
#include <iostream>

using namespace std;

struct ListNode
{
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution
{
public:
    ListNode *removeNthFromEnd(ListNode *head, int n)
    {
        ListNode *node = new ListNode(0);
        ListNode *slow = node;
        slow->next = head;
        while(n>0)
        {
            head = head->next;
            n--;
        }
        while(head)
        {
            head=head->next;
            slow=slow->next;
        }
        if(slow->next==nullptr)
        {
            slow->next=nullptr;
        }else{
            slow->next=slow->next->next;
        }
        return node->next;
    }
    // ListNode *removeNthFromEnd(ListNode *head, int n)
    // {
    //     // map n to index
    //     vector<int> tmpVec;
    //     ListNode *tmpNode = head;
    //     while(tmpNode->next!=nullptr)
    //     {
    //         int num = tmpNode->val;
    //         // cout <<  num << ", ";
    //         tmpVec.push_back(num);
    //         tmpNode = tmpNode->next;
    //     }
    //     tmpVec.push_back(tmpNode->val);
    //     int nIndex = tmpVec.size()-n;
    //     // cout <<endl;
    //     // cout << "tmpVec size: " << tmpVec.size() << endl;
    //     // for(auto num: tmpVec)
    //     //     cout << num << endl;
    //     // ListNode *currentNodeHead = new ListNode(tmpVec[tmpVec.size()-1]);
    //     if (tmpVec.size()==1)
    //         return nullptr;
    //     ListNode *currentNodeHead = new ListNode();
    //     for(int i=tmpVec.size()-1; i>=0; i--)
    //     {
    //         // cout << tmpVec[i] << ", ";
    //         if(i==nIndex)
    //         {
    //             continue;
    //         }
    //         currentNodeHead->val = tmpVec[i];
    //         if(i==0)
    //         {
    //             continue;
    //         }
    //         ListNode *nextNode = new ListNode();
    //         nextNode->next = currentNodeHead;
    //         currentNodeHead = nextNode;
    //     }
    //     return currentNodeHead;
    // }
    // ListNode *removeNthFromEnd(ListNode *head, int n)
    // {
    //     int counter = 1;
    //     ListNode *currentNode;
    //     currentNode = head;
    //     while (counter != n - 1)
    //     {
    //         currentNode = currentNode->next;
    //         counter++;
    //     }
    //     ListNode *removeNode = currentNode->next;
    //     ListNode *continueNode = removeNode->next;
    //     currentNode->next = continueNode;
    //     return head;
    // }
};

void printList(ListNode *myList)
{
    if (myList==nullptr)
    {
        cout << "List is empty" << endl;
        return;
    }
    cout << myList->val << ", ";
    ListNode *currentNode = myList->next;
    while (currentNode != nullptr)
    {
        cout << currentNode->val << ", ";
        currentNode = currentNode->next;
    }
    cout << endl;
}

int main(int argc, char **argv)
{
    Solution sol;
    // ListNode *myList5 = new ListNode(5);
    // ListNode *myList4 = new ListNode(4, myList5);
    // ListNode *myList3 = new ListNode(3, myList4);
    // ListNode *myList2 = new ListNode(2, myList3);
    ListNode *myList2 = new ListNode(2);
    ListNode *myList1 = new ListNode(1, myList2);
    auto newList = sol.removeNthFromEnd(myList1, 2);
    // cout << "hello world" << endl;
    printList(newList);
    return 0;
}