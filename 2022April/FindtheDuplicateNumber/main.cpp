#include <vector>
#include <iostream>

using namespace std;

class Solution
{
public:
    int findDuplicate(vector<int> &nums)
    {
        vector<int> sortedNumVec(nums.size()-1, -1);
        for(size_t i=0; i < nums.size(); i++)
        {
            if(sortedNumVec[nums[i]-1]!=-1)
            {
                return nums[i];
            }else{
                sortedNumVec[nums[i]-1] = i;
            }
            
        }
        return 0;
    }
};

int main(int argc, char *argv[])
{
    Solution sol;
    vector<int> nums1 = {1, 3, 4, 2, 2};
    vector<int> nums2 = {3, 1, 3, 4, 2};
    auto output = sol.findDuplicate(nums1);
    std::cout << "test case 1: " << output << std::endl;
    output = sol.findDuplicate(nums2);
    std::cout << "test case 2: " << output << std::endl;
    return 0;
}