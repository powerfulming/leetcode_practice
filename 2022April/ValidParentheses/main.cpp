#include <iostream>
#include <stack>

using namespace std;

class Solution
{
public:
    bool isValid(string s)
    {
        bool start = false;
        stack<char> parentheses;
        for (auto myC : s)
        {
            if (myC == '(' || myC == '[' || myC == '{')
            {
                start = true;
                parentheses.push(myC);
            }
            else if (start)
            {
                if (parentheses.top() == '(' && myC == ')')
                {
                    parentheses.pop();
                    if(parentheses.size()==0)
                        start=false;
                }
                else if (parentheses.top() == '[' && myC == ']')
                {
                    parentheses.pop();
                    if(parentheses.size()==0)
                        start=false;
                }
                else if (parentheses.top() == '{' && myC == '}')
                {
                    parentheses.pop();
                    if(parentheses.size()==0)
                        start=false;
                }
                else
                {
                    cout << "wrong" << endl;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        if (parentheses.size() != 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
};

int main(int argc, char **argv)
{
    Solution sol;
    // string testCase = "()[]{}";
    // string testCase = "(}";
    // string testCase = "{[]}";
    // string testCase = "{";
    string testCase = "(){}}{";
    auto result = sol.isValid(testCase);
    cout << "result: " << result << endl;
    return 0;
}