#include <iostream>
#include <vector>
#include <map>

using namespace std;

class Solution
{
public:
    void Soulution()
    {
    }
    void buildDataSet()
    {
        vector<int> splitHelper = {3, 3, 3, 3, 3, 4, 3, 4};
        size_t index = 97;
        for (size_t i = 2; i <= 9; i++)
        {
            vector<int> tmpVec;
            // cout << i << ":  ";
            for (size_t k = 0; k < splitHelper[i - 2]; k++)
            {
                // cout << index << ", ";
                tmpVec.push_back(index);
                index++;
            }
            // cout << endl;
            char charIndex = i + '0';
            data_set_[charIndex] = tmpVec;
        }
    }
    vector<string> letterCombinations(string digits)
    {
        vector<string> answer;
        for (char c : digits)
        {
            vector<string> tmpDigitsAlphabet;
            vector<string> tmpAnswer;
            // Investigate input digit's corresponding alphabet
            for (size_t i = 0; i < data_set_[c].size(); i++)
            {
                tmpDigitsAlphabet.push_back(to_string(static_cast<char>(data_set_[c][i])));
                // cout << static_cast<char>(data_set_[c][i]) << endl;
            }
            if(answer.size()==0)
            {
                answer = tmpDigitsAlphabet;
                continue;
            }
            for(size_t i=0; i<answer.size(); i++)
            {
                for(size_t j=0; j<tmpDigitsAlphabet.size(); j++)
                {
                    tmpAnswer;
                }
            }
            answer = tmpAnswer;
        }
        return answer;
    }
    map<char, vector<int>> data_set_;
};

int main(int argc, char **argv)
{
    // ASCii
    // char testC = static_cast<char>(97);
    // cout << testC << endl;

    // int to char
    // int i=5;
    // char c = i+'0';
    // cout << "c: " << c << endl;

    Solution sol;
    string testCase1 = "2";
    sol.buildDataSet();
    auto answer = sol.letterCombinations(testCase1);
    for(auto str: answer)
        cout << str << ", ";
    cout << endl;
    return 0;
}