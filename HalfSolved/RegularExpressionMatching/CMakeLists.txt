cmake_minimum_required(VERSION 3.10.0)
project(RegularExpressionMatching)
set(CMAKE_CXX_STANDARD 14)

################################
# GTest
################################
find_package(GTest REQUIRED)
include_directories(${GTEST_INCLUDE_DIRS})

################################
# Unit Tests
################################
# Add test cpp file
add_executable(${PROJECT_NAME}_test main.cpp )
target_link_libraries(${PROJECT_NAME}_test 
    ${GTEST_BOTH_LIBRARIES}
    pthread
)