// Given an input string s and a pattern p, implement regular expression matching with support for '.' and '*' where:

// '.' Matches any single character.​​​​
// '*' Matches zero or more of the preceding element.
// The matching should cover the entire input string (not partial).

// Check Pattern size first

#include <iostream>
#include <gtest/gtest.h>

using namespace std;

// class Solution {
// public:
//     bool isMatch(string s, string p) {
//         if (p.empty()) return s.empty();
//         if (p.size() > 1 && p[1] == '*') {
//             return isMatch(s, p.substr(2)) || (!s.empty() && (s[0] == p[0] || p[0] == '.') && isMatch(s.substr(1), p));
//         } else {
//             return !s.empty() && (s[0] == p[0] || p[0] == '.') && isMatch(s.substr(1), p.substr(1));
//         }
//     }
// };

// 如遇到 "*" p往前看一格，如與s一樣則 S的 index+1, 如不一樣則p的index+1
class Solution
{
public:
    bool isMatch(string s, string p)
    {
        if (s.empty())
        {
            if (p.empty())
                return true;
            else
            {
                if (p[0] == '*')
                {
                    return isMatch(s, p.substr(1));
                }
                if(star_appear_)
                {
                    if(char_before_star_==p[0])
                    {
                        star_appear_ = false;
                        return isMatch(s, p.substr(1));
                    }
                    
                }
                if(p[1]=='*')
                {

                }
                return false; // double check here
            }
        }
        if (p.empty())
            return false;

        if (s[0] == p[0]) // 完美情況，往下繼續檢查
        {
            char_before_star_ = p[0];
            if(p.size()>=2)
            {
                if(p[1]=='*')
                {
                    
                }
            }
            return isMatch(s.substr(1), p.substr(1));
        }
        else
        {
            if (p[0] == '.')
            {
                // 萬用字符(wildcard character)，完沒結合，繼續一起往後看
                char_before_star_ = p[0];
                return isMatch(s.substr(1), p.substr(1));
            }
            else if (p[0] != '*')
            {
                char_before_star_ = p[0];
                return isMatch(s, p.substr(1));
            }
            else if (p[0] == '*')
            {
                star_appear_ = true;
                if (char_before_star_ == '.')
                {
                    // aabc
                    // .*c
                    if (p.size() >= 2)
                    {
                        if(s.size()>=2)
                        {
                            if(s[1]==p[1])
                            {
                                return isMatch(s.substr(1), p.substr(1));
                            }
                            else
                                return false;
                        }else
                            return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                if (char_before_star_ == s[0])
                {
                    bool result1 = isMatch(s.substr(1), p);
                    bool result2 = isMatch(s, p.substr(1));
                    // return isMatch(s.substr(1), p) || isMatch(s, p.substr(1));
                    return  result1||result2 ;
                }else{
                    return isMatch(s, p.substr(1));
                }
            }
        }
        return false;
    }

private:
    char char_before_star_;
    bool star_appear_=false;
    int counter_=0;
};

TEST(testCase, test1)
{
    Solution sol;
    string s = "aa";
    string p = "a";
    EXPECT_EQ(sol.isMatch(s, p), false);
}

TEST(testCase, test2)
{
    Solution sol;
    string s = "aa";
    string p = "a*";
    EXPECT_EQ(sol.isMatch(s, p), true);
}

TEST(testCase, test3)
{
    Solution sol;
    string s = "mississippi";
    string p = "mis*is*p*.";
    EXPECT_EQ(sol.isMatch(s, p), false);
}

TEST(testCase, test4)
{
    Solution sol;
    string s = "ab";
    string p = ".*";
    EXPECT_EQ(sol.isMatch(s, p), true);
}

TEST(testCase, test5)
{
    Solution sol;
    string s = "aab";
    string p = "c*a*b";
    EXPECT_EQ(sol.isMatch(s, p), true);
}
TEST(testCase, test6)
{
    Solution sol;
    string s = "";
    string p = "";
    EXPECT_EQ(sol.isMatch(s, p), true);
}
TEST(testCase, test7)
{
    Solution sol;
    string s = "s";
    string p = "";
    EXPECT_EQ(sol.isMatch(s, p), false);
}
TEST(testCase, test8)
{
    Solution sol;
    string s = "";
    string p = "s";
    EXPECT_EQ(sol.isMatch(s, p), false);
}
TEST(testCase, test9)
{
    Solution sol;
    string s = "";
    string p = ".";
    EXPECT_EQ(sol.isMatch(s, p), false);
}
TEST(testCase, test10)
{
    Solution sol;
    string s = "";
    string p = "*";
    EXPECT_EQ(sol.isMatch(s, p), true);
}
TEST(testCase, test11)
{
    Solution sol;
    string s = "ab";
    string p = ".*c";
    EXPECT_EQ(sol.isMatch(s, p), false);
}
TEST(testCase, test12)
{
    Solution sol;
    string s = "abc";
    string p = ".*c";
    EXPECT_EQ(sol.isMatch(s, p), true);
}
TEST(testCase, test13)
{
    Solution sol;
    string s = "aaa";
    string p = "a*a";
    EXPECT_EQ(sol.isMatch(s, p), true);
}
TEST(testCase, test14)
{
    Solution sol;
    string s = "aaa";
    string p = "aaaa";
    EXPECT_EQ(sol.isMatch(s, p), false);
}
TEST(testCase, test15)
{
    Solution sol;
    string s = "aaaa";
    string p = "a*a*";
    EXPECT_EQ(sol.isMatch(s, p), true);
}
TEST(testCase, test16)
{
    Solution sol;
    string s = "aaaa";
    string p = "a*aa";
    EXPECT_EQ(sol.isMatch(s, p), true);
}
TEST(testCase, test17)
{
    Solution sol;
    string s = "a";
    string p = "ab*";
    EXPECT_EQ(sol.isMatch(s, p), true);
}
TEST(testCase, test18)
{
    Solution sol;
    string s = "aaaa";
    string p = "aaa*aaa*";
    EXPECT_EQ(sol.isMatch(s, p), true);
}
int main(int argc, char *argv[])
{
    // testing::InitGoogleTest(&argc, argv);
    // return RUN_ALL_TESTS();
    Solution sol;
    // string s = "a";
    // string p = "ab*";
    string s = "aa";
    string p = "a*aaa*";
    if(sol.isMatch(s,p))
        cout << "Match!" << endl;
    else
        cerr << "error" << endl;
    return 0;
}