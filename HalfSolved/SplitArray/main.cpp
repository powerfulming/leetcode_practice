#include <vector>
#include <map>
#include <iostream>
#include <stack>

using namespace std;

class Solution
{
public:
    int splitArray(vector<int> &nums, int m)
    {
        for (size_t i = 0; i < m+1; i++)
        {
            if(i==m)
            {
                index_[i] = -1;
            }
            else
            {
                index_[i] = i;
            }
        }
        while (index_[0] != (nums.size() - m))
        {
            int currIndex = 0;
            for(int i=(m-1); i>=0; i--)
            {
                stack<int> myStack;
                int tmpSum=0;
                int tmpBeginIndex=index_[i];
                while(index_[i]<nums.size() || index_[i]<index_[i+1])
                {
                    tmpSum+=nums[index_[i]];
                    index_[i]++;
                }
                if((tmpBeginIndex+1)<m || (tmpBeginIndex+1)<index_[i+1])
                    tmpBeginIndex++;
                index_[i]=tmpBeginIndex;
                myStack.push(tmpSum);
            }
        }
    }

private:
    map<int, int> index_;
    unsigned int largest_num_ = 0;
};

int main(int argc, char *argv[])
{
    Solution sol;
    vector<int> nums1 = {1, 3, 4, 2, 2};
    vector<int> nums2 = {3, 1, 3, 4, 2};
    auto output = sol.splitArray(nums1);
    std::cout << "test case 1: " << output << std::endl;
    output = sol.splitArray(nums2);
    std::cout << "test case 2: " << output << std::endl;
    return 0;
}