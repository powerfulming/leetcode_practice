#include <iostream>
#include <vector>
#include <gtest/gtest.h>

using namespace std;

class Solution
{
public:
    int maxArea(vector<int> &height)
    {
        // 掃描線，分別從左邊與右邊掃往中間
        // Tips: 只移動第二高的掃描線
        size_t scanL = 0, scanR = height.size() - 1;
        int maxArea = 0;
        while (scanL < scanR)
        {
            size_t leftHeight = height[scanL];
            size_t rightHeight = height[scanR];
            size_t secondHeightest = min(leftHeight, rightHeight);
            
            int area = secondHeightest * (scanR - scanL);
            if (area > maxArea)
            {
                maxArea = area;
            }
            if(leftHeight>rightHeight)
            {
                scanR--;
            }else{
                scanL++;
            }
        }
        return maxArea;
    }
};

TEST(testCase, test1)
{
    Solution sol;
    vector<int> v = {1, 8, 6, 2, 5, 4, 8, 3, 7};
    EXPECT_EQ(sol.maxArea(v), 49);
}

TEST(testCase, test2)
{
    Solution sol;
    vector<int> v = {1, 1};
    EXPECT_EQ(sol.maxArea(v), 1);
}


TEST(testCase, test3)
{
    Solution sol;
    vector<int> v = {4, 3, 2, 1, 4};
    EXPECT_EQ(sol.maxArea(v), 16);
}

TEST(testCase, test4)
{
    Solution sol;
    vector<int> v = {1, 2, 1};
    EXPECT_EQ(sol.maxArea(v), 2);
}


TEST(testCase, test5)
{
    Solution sol;
    vector<int> v = {1, 100, 100, 1, 4};
    EXPECT_EQ(sol.maxArea(v), 100);
}

int main(int argc, char *argv[])
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
    // Solution sol;

    return 0;
}