#include <iostream>
#include <gtest/gtest.h>
#include <chrono>
#include <limits.h>

using namespace std;

class Solution
{
public:
    int myAtoi(string s)
    {
        int output = 0;
        bool mySigned = true;
        bool processAnyCharacter = false;
        for (auto c : s)
        {
            if (c < 48 || c > 57)
            {
                if (c == 43) // '+'
                {
                    if (processAnyCharacter)
                    {
                        if (output != 0)
                        {
                            if (!mySigned)
                                output = output * (-1);
                        }
                        else
                        {
                            output = 0;
                        }
                        return output;
                    }
                    else
                        mySigned = true;
                    processAnyCharacter = true;
                    continue;
                }
                else if (c == 45) // '-'
                {
                    if (processAnyCharacter)
                    {
                        if (output != 0)
                        {
                            if (!mySigned)
                                output = output * (-1);
                        }
                        else
                        {
                            output = 0;
                        }
                        return output;
                    }
                    else
                        mySigned = false;
                    processAnyCharacter = true;
                    continue;
                }
                else if (c == 32) // ' ' //space
                {
                    if (!processAnyCharacter)
                        continue;
                    else
                    {
                        if (!mySigned)
                            output = output * (-1);
                        return output;
                    }
                }
                else
                {
                    processAnyCharacter = true;
                    // cout << "not number" << endl;
                    if (!mySigned)
                        output = output * (-1);
                    return output;
                }
            }
            if (output > INT_MAX / 10)
            {
                if (mySigned)
                    return INT_MAX;
                else
                    return INT_MIN;
            }
            else if (output == INT_MAX / 10)
            {
                if ((c - 48) > 7 && mySigned)
                {
                    return INT_MAX;
                }
                else if ((c - 48) >= 8 && !mySigned)
                {
                    return INT_MIN;
                }
                else
                {
                }
            }
            output = output * 10;
            output += (c - 48);
            processAnyCharacter = true;
        }
        if (!mySigned)
            output = output * (-1);
        return output;
    }
};

TEST(testCase, test1)
{
    Solution sol;
    string s = "123";
    EXPECT_EQ(sol.myAtoi(s), 123);
}

TEST(testCase, test2)
{
    Solution sol;
    string s = "   42";
    EXPECT_EQ(sol.myAtoi(s), 42);
}

TEST(testCase, test3)
{
    Solution sol;
    string s = "-91";
    EXPECT_EQ(sol.myAtoi(s), -91);
}

TEST(testCase, test4)
{
    Solution sol;
    string s = "69 my favorite pose";
    EXPECT_EQ(sol.myAtoi(s), 69);
}

TEST(testCase, test5)
{
    Solution sol;
    string s = "words and 987";
    EXPECT_EQ(sol.myAtoi(s), 0);
}
TEST(testCase, test6)
{
    Solution sol;
    string s = "-91283472332";
    EXPECT_EQ(sol.myAtoi(s), INT_MIN);
}
TEST(testCase, test7)
{
    Solution sol;
    string s = "9999999999";
    EXPECT_EQ(sol.myAtoi(s), INT_MAX);
}
TEST(testCase, test8)
{
    Solution sol;
    string s = "3.14";
    EXPECT_EQ(sol.myAtoi(s), 3);
}
TEST(testCase, test9)
{
    Solution sol;
    string s = "+-12";
    EXPECT_EQ(sol.myAtoi(s), 0);
}
TEST(testCase, test10)
{
    Solution sol;
    string s = "-+12";
    EXPECT_EQ(sol.myAtoi(s), 0);
}
TEST(testCase, test11)
{
    Solution sol;
    string s = "  -0012a42";
    EXPECT_EQ(sol.myAtoi(s), -12);
}
TEST(testCase, test12)
{
    Solution sol;
    string s = "-2147483648";
    EXPECT_EQ(sol.myAtoi(s), -2147483648);
}
TEST(testCase, test13)
{
    Solution sol;
    string s = "    -88827   5655  U";
    EXPECT_EQ(sol.myAtoi(s), -88827);
}
TEST(testCase, test14)
{
    Solution sol;
    string s = "-5-";
    EXPECT_EQ(sol.myAtoi(s), -5);
}
int main(int argc, char *argv[])
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
    // Solution sol;
    // string s = "-2147483648";
    // cout << sol.myAtoi(s) << endl;

    return 0;
}