#include <iostream>
#include <vector>
#include <gtest/gtest.h>

using namespace std;

class Solution
{
public:
    double findMedianSortedArrays(vector<int> &nums1, vector<int> &nums2)
    {
        double output = 0.0;
        size_t mergeVectorSize = nums1.size()+nums2.size();
        size_t counter=0;
        size_t counterA=0;
        size_t counterB=0;
        vector<int> mergeVector;
        int tmpNum1 = 0.0;
        int tmpNum2 = 0.0;
        while(counter!=mergeVectorSize)
        {
            if(nums1.empty())
            {
                mergeVector = nums2;
                break;
            }
            if(nums2.empty())
            {
                mergeVector = nums1;
                break;
            }
            if(mergeVectorSize==0)
            {
                output = 0.0;
                return output;
            }
            if(!nums1.empty() && counterA<nums1.size())
            {
                tmpNum1 = nums1[counterA];
            }
            if(!nums2.empty() && counterB<nums2.size())
            {
                tmpNum2 = nums2[counterB];
            }

            if(counterA>=nums1.size())
            {
                if(counterB>=nums2.size())
                {
                    cerr << "Impossible!!!" << endl;
                }else{
                    mergeVector.emplace_back(tmpNum2);
                    counterB++;
                    counter++;
                    continue;
                }
            }
            if(counterB>=nums2.size())
            {
                if(counterA>=nums1.size())
                {
                    cerr << "Impossible!!!" << endl;
                }else{
                    mergeVector.emplace_back(tmpNum1);
                    counterA++;
                    counter++;
                    continue;
                }
            }
            if(tmpNum1<=tmpNum2)
            {
                counterA++;
                mergeVector.emplace_back(tmpNum1);
            }else{
                counterB++;
                mergeVector.emplace_back(tmpNum2);
            }
            counter++;
        }
        // cout << "---------------------------------" << endl;
        // for(auto num: mergeVector)
        // {
        //     cout << num << " ";
        // }
        // cout << endl;
        // cout << "---------------------------------" << endl;
        size_t sizeQuotient = mergeVectorSize/2;
        size_t sizeRemainder = mergeVectorSize%2;
        // cout << "sizeQuotient: " << sizeQuotient << endl;
        // cout << "sizeRemainder: " << sizeRemainder << endl;
        if(sizeRemainder==0)
        {
            // cout << mergeVector[sizeQuotient-1] << " + " << mergeVector[sizeQuotient] << " = " << endl;
            output = (mergeVector[sizeQuotient-1]+mergeVector[sizeQuotient])/2.0;
        }
        if(sizeRemainder==1)
        {
            output = mergeVector[sizeQuotient];
        }
        return output;
    }
};

TEST(testCase, test1)
{
    vector<int> nums1 = {1, 3};
    vector<int> nums2 = {2};
    Solution sol;
    EXPECT_EQ(sol.findMedianSortedArrays(nums1, nums2), 2.0);
}
TEST(testCase, test2)
{
    vector<int> nums1 = {1, 2};
    vector<int> nums2 = {3, 4};
    Solution sol;
    EXPECT_EQ(sol.findMedianSortedArrays(nums1, nums2), 2.5);
}
TEST(testCase, test3)
{
    vector<int> nums1 = {0, 0};
    vector<int> nums2 = {0, 0};
    Solution sol;
    EXPECT_EQ(sol.findMedianSortedArrays(nums1, nums2), 0.0);
}
TEST(testCase, test4)
{
    vector<int> nums1 = {};
    vector<int> nums2 = {1};
    Solution sol;
    EXPECT_EQ(sol.findMedianSortedArrays(nums1, nums2), 1.0);
}
TEST(testCase, test5)
{
    vector<int> nums1 = {2};
    vector<int> nums2 = {};
    Solution sol;
    EXPECT_EQ(sol.findMedianSortedArrays(nums1, nums2), 2.0);
}
int main(int argc, char *argv[])
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
    return 0;
}