| P   |     | A   |     | H   |     | N   |
| --- | --- | --- | --- | --- | --- | --- |
| A   | P   | L   | S   | I   | I   | G   |
| Y   |     | I   |     | R   |     |     |

```sh
P       A       H       N
A   P   L   S   I   I   G
Y       I       R
```