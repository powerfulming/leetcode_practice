#include <iostream>
#include <gtest/gtest.h>
#include <chrono>

using namespace std;

class Solution
{
public:
    string convert(string s, int numRows)
    {
        int maxJumpValue = (numRows == 1) ? 1 : numRows * 2 - 2;
        int incr1, incr2;
        string output="";
        for (int i=0; i<numRows; i++)
        {
            // if(maxJumpValue>=0 && )
            if(i!=0 || i!=(numRows-1))
            {
                while()
                output+=s[maxJumpValue*i];
            }
            maxJumpValue -= 2;
        }
        
    }
};

class Solution2
{
public:
    string convert(string s, int numRows)
    {
        int max_increase; //最大跳值
        int inc0, inc1;   //2個跳值
        int flag;         //讓跳值輪流用

        string ans = "";

        max_increase = (numRows == 1) ? 1 : numRows * 2 - 2;

        for (int i = 0; i < numRows; i++)
        {

            int j = i;

            inc0 = (i == numRows - 1) ? max_increase : max_increase - i * 2; //跳值1
            inc1 = (i == 0) ? max_increase : i * 2;                          //跳值2

            flag = 0;

            while (j < s.length())
            {
                ans.push_back(s[j]); //儲存至ans

                j += (flag == 0) ? inc0 : inc1; //輪流跳

                flag = flag ^ 1; //用xor翻轉旗標 (0->1, 1->0)
            }
        }
        return ans;
    }
};

TEST(testCase, test1)
{
    Solution sol;
    string s = "PAYPALISHIRING";
    EXPECT_EQ(sol.convert(s, 3), "PAHNAPLSIIGYIR");
}

TEST(testCase, test2)
{
    Solution sol;
    string s = "PAYPALISHIRING";
    EXPECT_EQ(sol.convert(s, 4), "PINALSIGYAHRPI");
}

TEST(testCase, test3)
{
    Solution sol;
    string s = "A";
    EXPECT_EQ(sol.convert(s, 1), "A");
}

int main(int argc, char *argv[])
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
    return 0;
}