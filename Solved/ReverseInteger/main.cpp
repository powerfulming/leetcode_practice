#include <iostream>
#include <bits/stdc++.h>

using namespace std;

class Solution
{
public:
    int reverse(int x)
    {
        int quotient, remainder, output = 0;
        quotient = x;
        do
        {
            remainder = quotient % 10;
            quotient = quotient / 10;
            if (output > (INT_MAX / 10))
            {
                output = 0;
                return output;
            }
            else if (output == (INT_MAX / 10))
            {
                if (remainder > (INT_MAX % 10))
                {
                    output = 0;
                    return output;
                }
            }

            if (output < (INT_MIN / 10))
            {

                output = 0;
                return output;
            }
            else if (remainder == (INT_MIN / 10))
            {
                if (remainder < (INT_MIN % 10))
                {
                    output = 0;
                    return output;
                }
            }

            if ((output * 10 + remainder) > 0)
            {
                output = output * 10 + remainder;
            }
            else if ((output * 10 + remainder) < 0)
            {
                output = output * 10 + remainder;
            }
            else
            {
                output = 0;
            }
        } while (quotient != 0);
        return output;
    }
};

int main()
{
    Solution sol;
    auto answer = sol.reverse(-134236469);
    cout << "Answer: " << answer << endl;
}