#include <iostream>
#include <gtest/gtest.h>
#include <chrono>
using namespace std;

class Solution2 {
public:
  string longestPalindrome(string s) {
      if( s.length() < 1 ) return "";

      int start = 0, length = 1;
      for(int i = 0 ; i < s.length() ; ++i){
        int oddStringLength = findPalindromeFromCenter(s, i, i);
        int evenStringLength = findPalindromeFromCenter(s, i, i+1);
        int maxLength = max(oddStringLength, evenStringLength);
        if(maxLength > length){
          length = maxLength;
          start = i - (maxLength - 1) / 2;
        }
      }

      return s.substr(start, length);
  }

  int findPalindromeFromCenter(const string& s, int left, int right){
    while(left >= 0 && right < s.length() && s[left] == s[right]) left--, right++;
    return right - left - 1;
  }
};

class Solution3 {
public:
    string longestPalindrome(string s) {
        string output;
        string tmp;
        output  =s[0];
        
        for(size_t beginIndex=0; beginIndex<s.size()-1; beginIndex++)
        {
            for(size_t endIndex=beginIndex+1; endIndex<s.size()+1; endIndex++)
            {
                tmp.clear();
                for(size_t i=beginIndex; i<s.size();i++)
                {
                    if(i>=beginIndex && i<endIndex)
                    {
                        tmp += s[i];
                    }else if(i>=endIndex)
                    {
                        break;
                    }
                }
                if(tmp.size()>output.size())
                {
                    if(flip_string(tmp)==tmp)
                        output = tmp;
                }
            }
        }
        
        return output;
    }
    string flip_string(string s)
    {
        string flipStr;
        for(std::string::iterator it=s.end(); it!=s.begin();)
        {
            it--;
            flipStr += *it;
        }
        return flipStr;
    }
};

class Solution {
public:
    string longestPalindrome(string s) 
    {
        int startIndex=0;
        int maxLength=1;
        for(int i=0; i<s.size(); i++)
        {
            int oddPalidormicLength = find_palidormic_from_center(s, i, i);
            int evenPalidormicLength = find_palidormic_from_center(s, i, i+1);
            int tmpMaxLength = max(oddPalidormicLength, evenPalidormicLength);
            if(tmpMaxLength>maxLength)
            {
                maxLength = tmpMaxLength;
                startIndex = i - (maxLength-1)/2;
            }
        }
        return s.substr(startIndex, maxLength);
    }
    int find_palidormic_from_center(string s, int leftIndex, int rightIndex)
    {
        while(leftIndex>=0 && rightIndex<s.size() && s[leftIndex]==s[rightIndex])
        {
            leftIndex--, rightIndex++;
        }
        return rightIndex - leftIndex - 1;
    }
};

TEST(testCase, test1)
{
    string s="babad";
    Solution sol;
    EXPECT_EQ(sol.longestPalindrome(s), "bab");
}

TEST(testCase, test2)
{
    string s="cbbd";
    Solution sol;
    EXPECT_EQ(sol.longestPalindrome(s), "bb");
}

TEST(testCase, test3)
{
    string s="a";
    Solution sol;
    EXPECT_EQ(sol.longestPalindrome(s), "a");
}

TEST(testCase, test4)
{
    string s="ac";
    Solution sol;
    EXPECT_EQ(sol.longestPalindrome(s), "a");
}


int main(int argc, char* argv[])
{
    // Solution sol;
    // auto t0 = std::chrono::high_resolution_clock::now();
    // // string result = sol.longestPalindrome("jglknendplocymmvwtoxvebkekzfdhykknufqdkntnqvgfbahsljkobhbxkvyictzkqjqydczuxjkgecdyhixdttxfqmgksrkyvopwprsgoszftuhawflzjyuyrujrxluhzjvbflxgcovilthvuihzttzithnsqbdxtafxrfrblulsakrahulwthhbjcslceewxfxtavljpimaqqlcbrdgtgjryjytgxljxtravwdlnrrauxplempnbfeusgtqzjtzshwieutxdytlrrqvyemlyzolhbkzhyfyttevqnfvmpqjngcnazmaagwihxrhmcibyfkccyrqwnzlzqeuenhwlzhbxqxerfifzncimwqsfatudjihtumrtjtggzleovihifxufvwqeimbxvzlxwcsknksogsbwwdlwulnetdysvsfkonggeedtshxqkgbhoscjgpiel");
    // // string result = sol.longestPalindrome("abaaaa");
    // string result = sol.longestPalindrome("abbb");
    // auto t1 = std::chrono::high_resolution_clock::now();
    // auto dt = 1.e-9 * std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t0).count();
    // cout << "find " << result << " and spend " << dt << " seconds" << endl;


    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
    return 0;
}