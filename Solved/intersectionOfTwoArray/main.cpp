#include <vector>
#include <iostream>

using namespace std;

class Solution
{
public:
    vector<int> intersect(vector<int> &nums1, vector<int> &nums2)
    {
        vector<int> output;
        my_bubble_sort(nums1);
        my_bubble_sort(nums2);

        int i = 0, j = 0;
        for (; i < nums1.size(); i++)
        {
            for (; j < nums2.size();)
            {
                int num1 = nums1[i];
                int num2 = nums2[j];
                cout << "num1: " << nums1[i] << endl;
                cout << "num2: " << nums2[j] << endl;
                cout << "i: " << i << " j: " << j << endl;
                if (num1 < num2)
                {
                    cout << num2 << " bigger than " << num1 << ", dont need to compare" << endl;
                    break;
                }
                if (num1 == num2)
                {
                    cout << "bingo" << endl;
                    output.emplace_back(num1);
                    j++;
                    break;
                }
                if(num1>num2)
                    j++;
            }
        }
        return output;
    }
    void my_bubble_sort(std::vector<int> &myVector)
    {
        int i = 0, j = 0, counter = 0;
        for (i = 0; i < myVector.size(); i++)
        {
            for (j = 0; j < myVector.size() - 1 - i; j++)
            {
                if (myVector[j] > myVector[j + 1])
                {
                    m_swap(myVector[j], myVector[j + 1]);
                }
            }
        }
    }
    void m_swap(int &a, int &b)
    {
        a = a + b;
        b = a - b;
        a = a - b;
    }
    static void print_vec(std::vector<int> &myVector)
    {
        for (auto num : myVector)
        {
            cout << num << " ";
        }
        cout << endl;
    }
};

int main(int argc, char *argv[])
{
    Solution sol;
    vector<int> nums1 = {1, 2, 2, 1};
    vector<int> nums2 = {2, 2};
    auto output = sol.intersect(nums1, nums2);
    Solution::print_vec(output);
    return 0;
}