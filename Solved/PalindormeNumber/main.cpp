#include <iostream>
#include <gtest/gtest.h>
#include <cmath>

using namespace std;

class Solution
{
public:
    bool isPalindrome(int x)
    {
        if (x < 0)
            return false;
        if (x == 0)
            return true;
        int digitNum = howManyDigit(x);
        if (digitNum == 1)
            return true;
        int reverseHalfNumber = 0;
        int dividend = 1;
        reverseHalfNumber = calculateReverseHalfNumber(x, digitNum/2);
        if(digitNum%2==0)
        {
            dividend = pow(10,digitNum/2);
        }else{
            dividend = pow(10, digitNum/2+1);
        }
        int firstHalfNumber = x/dividend;
        if(firstHalfNumber==reverseHalfNumber)
            return true;
        else
            return false;
    }
    int howManyDigit(int num)
    {
        return int(log10(num) + 1);
    }
    int calculateReverseHalfNumber(int inputNumber, int halfIndex)
    {
        halfIndex--;
        int output=0;
        for(size_t i=0; i<=halfIndex; i++)
        {
            output = output*10;
            output += inputNumber%10;
            inputNumber = inputNumber/10;
        }
        return output;
    }
};

TEST(testCase, test1)
{
    Solution sol;
    int s = 121;
    EXPECT_EQ(sol.isPalindrome(s), true);
}

TEST(testCase, test2)
{
    Solution sol;
    int s = 11;
    EXPECT_EQ(sol.isPalindrome(s), true);
}

TEST(testCase, test3)
{
    Solution sol;
    int s = 1001;
    EXPECT_EQ(sol.isPalindrome(s), true);
}

TEST(testCase, test4)
{
    Solution sol;
    int s = -121;
    EXPECT_EQ(sol.isPalindrome(s), false);
}
int main(int argc, char *argv[])
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
    // Solution sol;
    // int s = 12321;
    // cout << sol.calculateReverseHalfNumber(s, 2) << endl;
    // int ss = 1221;
    // cout << sol.calculateReverseHalfNumber(ss, 2) << endl;
    return 0;
}